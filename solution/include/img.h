#ifndef IMG_H
#define IMG_H

#include <inttypes.h>
#include <stdbool.h>

struct image {
	uint64_t width, height;
	struct pixel* data;
};

struct pixel {
	uint8_t b, g, r;
};

struct image create_img(uint32_t width, uint32_t height);
void delete_img(struct image* img);
struct pixel get_pixel(struct image const* img, uint32_t x, uint32_t y);
bool set_pixel(struct image* img, struct pixel const pix, uint32_t x, uint32_t y);
uint32_t img_size(struct image const* img);

#endif
