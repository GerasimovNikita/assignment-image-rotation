#ifndef BMP_HEADER_H
#define BMP_HEADER_H
#include "img.h"
#include <stdint.h>

#define TYPE 0x4D42
#define COMPRESSION 0
#define OFFBIT 54
#define RESERVED 0
#define PLANES 1
#define BIT_COUNT 24
#define BI_SIZE 40
#define X_PPM 0
#define Y_PPM 0
#define COL_USED 0
#define COL_IMP 0

struct __attribute__((packed)) bmp_header {
	uint16_t bfType;
	uint32_t bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
};

struct bmp_header img_header(const struct image* img);

#endif
