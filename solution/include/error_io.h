#ifndef ERROR_IO_H
#define ERROR_IO_H

enum error_code {
	ERROR_OPEN_FILES,
	ERROR_CLOSE_FILES,
	ERROR_WRITE_FILE,
	ERROR_READ_FILE,
	ERROR_ARGS
};

void print_error(enum error_code err_code);

#endif
