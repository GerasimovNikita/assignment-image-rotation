#ifndef PADDING_H
#define PADDING_H
#include "img.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

uint32_t find_padding(uint32_t width);
uint32_t padding_size(struct image const* img);
bool add_padding(FILE* out, uint32_t width);
#endif
