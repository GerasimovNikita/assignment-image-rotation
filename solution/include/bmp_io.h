#ifndef BMP_IO_H
#define BMP_IO_H
#include "img.h"
#include <stdio.h>

enum read_status {
	READ_OK = 0,
	READ_INVALID_HEADER,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_IMG_SIZE,
	READ_NULL_POINT,
	MALLOC_ERROR
};

enum write_status {
	WRITE_OK = 0,
	WRITE_ERROR,
	WRITE_NULL_POINT,
	WRITE_PAD_ERROR
};

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status to_bmp(FILE* out, struct image const* img);

#endif
