#include "../include/error_io.h"
#include <stdio.h>

static const char* error_msgs[] = {
	[ERROR_ARGS] = "Incorrect program arguements.",
	[ERROR_READ_FILE] = "Problem with reading file.",
	[ERROR_OPEN_FILES] = "Error with opening files.",
	[ERROR_CLOSE_FILES] = "Error with closing files.",
	[ERROR_WRITE_FILE] = "Error with writing in file.",
};

void print_error(enum error_code err_code){
	fprintf(stderr, "%s %s", error_msgs[err_code], "\n");
}
