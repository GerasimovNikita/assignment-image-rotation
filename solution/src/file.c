#include "../include/file.h"
#include <stdbool.h>
#include <stdio.h>

bool open_file(FILE** in_file, const char* in_file_name, const char* mode){
	if (in_file_name == NULL){
		return false;
	}
	*in_file = fopen(in_file_name, mode);
	return *in_file;
}

bool close_file(FILE* in_file){
	if (in_file == NULL){
		return false;
	}
	return !fclose(in_file);
}
