#include "../include/right_rotation.h"
#include "../include/bmp_io.h"
#include "../include/error_io.h"
#include "../include/file.h"
#include <stdio.h>

int main (int argc, char** argv) {
	if (argc != 3) {
		print_error(ERROR_ARGS);
		return 1;
	}
	FILE* in = NULL;
	FILE* out = NULL;
	if (!(open_file(&in, argv[1], "rb") && open_file(&out, argv[2], "wb"))) {
        print_error(ERROR_OPEN_FILES);
        return 1;
    }
	struct image img = {0};
	enum read_status read_status = from_bmp(in, &img);
	if (read_status != READ_OK) {
		print_error(ERROR_READ_FILE);
		delete_img(&img);
		close_file(in);
		close_file(out);
		return 1;
	}
	struct image rotated_img = rotate(img);
	delete_img(&img);
	if (to_bmp(out, &rotated_img)) {
		print_error(ERROR_WRITE_FILE);
		delete_img(&rotated_img);
		close_file(in);
		close_file(out);
		return 1;
	}
	delete_img(&rotated_img);
	if (!(close_file(in) && close_file(out))){
		print_error(ERROR_CLOSE_FILES);
		return 1;
	}
	return 0;
}
