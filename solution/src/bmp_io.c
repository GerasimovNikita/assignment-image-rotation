#include "../include/bmp_io.h"
#include "../include/bmp_header.h"
#include "../include/padding.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

static enum read_status read_img(FILE* in, struct bmp_header header, struct image* img){
	if(!(header.biHeight > 0 && header.biWidth > 0)){
		return READ_INVALID_HEADER;
	}
	*img = create_img(header.biWidth, header.biHeight);
	if (!img->data){
		return MALLOC_ERROR;
	}
	fseek(in, header.bOffBits, SEEK_SET);
	struct pixel pix = {0};
	for (size_t i = 0; i<img->height; i++){
		for (size_t j =0; j<img->width; j++){
			if(!fread(&pix, sizeof(struct pixel), 1, in)){
				delete_img(img);
				return READ_INVALID_BITS;
			}
			set_pixel(img, pix, j, i);
		}
		fseek(in, find_padding(img->width), SEEK_CUR);
	}
	return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* img){
	if (in == NULL || img == NULL){
		return READ_NULL_POINT;
	}
	struct bmp_header header = {0};
	if (!fread(&header, sizeof(struct bmp_header), 1, in)){
		return READ_INVALID_HEADER;
	}
	if (header.bfType != TYPE){
		return READ_INVALID_SIGNATURE;
	}
	if (!(header.biWidth > 0 && header.biHeight > 0)){
		return READ_INVALID_IMG_SIZE;
	}
	return read_img(in, header, img);
}

static enum write_status write_img(FILE* out, struct image const* img){
	struct pixel pix;
	for (size_t i = 0; i<img->height; i++){
		for (size_t j = 0; j<img->width; j++){
			pix = get_pixel(img, j, i);
			if(!fwrite(&pix, sizeof(struct pixel), 1, out)){
				return WRITE_ERROR;
			}
		}
		if (!add_padding(out, img->width)){
			return WRITE_PAD_ERROR;
		}
	}
	return WRITE_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img){
	if (out == NULL || img == NULL){
		return WRITE_NULL_POINT;
	}
	struct bmp_header header = img_header(img);
	if (!fwrite(&header, sizeof(struct bmp_header), 1, out)){
		return WRITE_ERROR;
	}
	return write_img(out, img);
}
