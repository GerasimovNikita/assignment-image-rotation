#include "../include/padding.h"

uint32_t find_padding (uint32_t width){
	if (width % 4 == 0){
		return 0;
	} else {
		return 4 - ((width*sizeof(struct pixel)) % 4);
	}
}

uint32_t padding_size (struct image const* img){
	return img->height * img->width * find_padding(img->width);
}

bool add_padding(FILE* out, uint32_t width){
	size_t zero_padding = 0;
	if (fwrite(&zero_padding, find_padding(width), 1, out) == 0 && find_padding(width) != 0){
		return false;
	}
	return true;
}
