#include "../include/right_rotation.h"
#include "../include/img.h"

#include <stdint.h>
#include <stdio.h>

struct image rotate(struct image img_src){
	if (img_src.data == NULL){
		return create_img(0, 0);
	}
	struct image rotated_img = create_img(img_src.height, img_src.width);
	for (size_t i = 0; i<img_src.height; i++){
		for (size_t j = 0; j<img_src.width; j++){
			set_pixel(&rotated_img, get_pixel(&img_src, j, img_src.height - i-1), i, j);
		}
	}
	return rotated_img;
}
