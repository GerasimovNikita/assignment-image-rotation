#include "../include/img.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>

struct image create_img(uint32_t width, uint32_t height){
	struct image new_img = {0};
	new_img.width = width;
	new_img.height = height;
	new_img.data = malloc(width*height*sizeof(struct pixel));
	if (!new_img.data){
		new_img.width = 0;
		new_img.height = 0;
	}
	return new_img;
}

void delete_img(struct image* img){
	free(img->data);
}

struct pixel get_pixel(struct image const* img, uint32_t x, uint32_t y){
	return *(img->data + y * img->width + x);
}

static bool check_pixel(struct image const* img, uint32_t x, uint32_t y){
	return x >= 0 && x <= img->width && y >= 0 && y <= img->height;
}

bool set_pixel(struct image* img, struct pixel const pix, uint32_t x, uint32_t y){
	if(!check_pixel(img, x, y)){
		return false;
	}
	*(img->data + y * img->width + x) = pix;
	return true;
}

uint32_t img_size(struct image const* img){
	return img->width * img->height * sizeof(struct pixel);
}
