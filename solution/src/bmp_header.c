#include "../include/bmp_header.h"
#include "../include/padding.h"
#include <stdint.h>

struct bmp_header img_header(const struct image* img){
	struct bmp_header header = {0};
	header.bfType = TYPE;
	header.bfileSize = sizeof(struct bmp_header) + img_size(img) + padding_size(img);
	header.bfReserved = RESERVED;
	header.bOffBits = OFFBIT;
	header.biSize = BI_SIZE;
	header.biWidth = img->width;
	header.biHeight = img->height;
	header.biPlanes = PLANES;
	header.biBitCount = BIT_COUNT;
	header.biCompression = COMPRESSION;
	header.biSizeImage = header.bfileSize - OFFBIT;
	header.biXPelsPerMeter = X_PPM;
	header.biYPelsPerMeter = Y_PPM;
	header.biClrUsed = COL_USED;
	header.biClrImportant = COL_IMP;
	return header;
}
